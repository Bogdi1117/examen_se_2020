package ex2;

public class S2 extends Thread {

        S2(String name){
            super(name);
        }

        public void run(){
            for(int i=0;i<5;i++){
                System.out.println(getName() + "  "+i);
                try {
                    Thread.sleep(4 * 1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        public static void main(String[] args) {
            S2 s1 = new S2("EThread1");
            S2 s2 = new S2("EThread2");


            s1.run();
            s2.run();

        }


}
