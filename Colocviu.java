package motronea_bogdan_se_colocviu;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Colocviu extends JFrame {
    JButton jb;
    JTextField tf1;
    JTextField tf2;
    Colocviu()
    {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(500,500);
        this.setLayout(null);
        tf1 = new JTextField("add file");
        tf1.setBounds(110,120,80,20);

        add(tf1);
        tf2 = new JTextField("Add text");
        tf2.setBounds(220,120,80,20);

        add(tf2);
        jb = new JButton("Click to add");
        jb.setBounds(170,300,100,20);
        add(jb);
        setVisible(true);
        jb.addActionListener(new ButtonEvent());

    }
    public class ButtonEvent implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String text = tf2.getText();
            String name = tf1.getText();
            try {
                FileWriter fw = new FileWriter("src/" + name);
                PrintWriter pw = new PrintWriter(fw);
                pw.write(text);
                pw.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            }
    }

    public static void main(String[] args) {
        new Colocviu();

    }


}
